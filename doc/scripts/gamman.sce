// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = gamman(n)
  u=%eps/2
  y = (n.*u)./(1-n.*u)
endfunction

u=%eps/2
[n gamman(n)/u]

// Computing gamma-n for various values of n

u = %eps/2;
for i = 1:15
    n=i;
    g = 1/(1-n*u);
    mprintf("%d & %.17e & ",n,g);
    n=10^i;
    g = 1/(1-n*u);
    mprintf("10^{%d} & %.17e\\\\\n",i,g);
end

// Another bound
// See Higham's Stability and Accuracy of Numerical Algorithms
// Section 3.4 Notation for error analysis
// Lemma 3.4
u = %eps/2;
for i = 1:15
    n=i;
    g = 1/(1-n*u/2);
    mprintf("%d & %.17e & ",n,g);
    n=10^i;
    g = 1/(1-n*u/2);
    mprintf("10^{%d} & %.17e\\\\\n",i,g);
end
