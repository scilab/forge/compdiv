// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = gamman(n)
  u=%eps/2
  y = (n.*u)./(1-n.*u)
endfunction

u=%eps/2

// Computing a bound for theta_2-theta_3
// Checking that theta_2-theta_3 = theta_5
stacksize("max")
for N = floor(logspace(1,6,10))
  t2 = grand(N,1,'unf',-1,1).*gamman(2);
  t3 = grand(N,1,'unf',-1,1).*gamman(3);
  d = abs(t2-t3)/u;
  mprintf("%d %e\n",N,max(d))
end

// Computing a bound for 4*theta_2-5*theta_3
// Checking that 4*theta_2-5*theta_3 = 9*theta_5
stacksize("max")
for N = floor(logspace(1,7,20))
  t2 = grand(N,1,'unf',-1,1).*gamman(2);
  t3 = grand(N,1,'unf',-1,1).*gamman(3);
  d = abs(4*t2+5*t3)/u;
  mprintf("%d %f\n",N,max(d))
end

// Checking that (1+d1)(1+d2)...(1+dn)=1+tn
// for n=5, where abs(di)<= u, and abs(tn)<=gamman
stacksize("max")
for N = floor(logspace(1,6,10))
  d = grand(N,5,'unf',-1,1).*u;
  d = abs(prod(1+d,"c")-1)/u;
  mprintf("%d %f\n",N,max(d))
end


// Checking that (1+tj)(1+tk)=1+t(j+k)
// for j=2, k=3
stacksize("max")
for N = floor(logspace(1,6,10))
  tj = grand(N,1,'unf',-1,1).*gamman(2);
  tk = grand(N,1,'unf',-1,1).*gamman(3);
  d = abs((1+tj).*(1+tk)-1)/u;
  mprintf("%d %e\n",N,max(d))
end

// Checking an intermediate result for 
// (1+tk)/(1+tj) = 1+t(j+k), when j<=k.
u = %eps/2;
dmin = %inf;
for irand = 1 : 1000
 e = grand(1,1,"uin",1,29);
 j = 2^e;
 f = grand(1,1,"uin",e+1,30);
 k = 2^f;
  t = ((k+j)*u-2*k*j*u2)/(1-(2*j+k)*u+2*j*k*u2);
  s = ((k+j)*u)/(1-(k+j)*u);
  // We want to prove that t<=s, ie
  // d = s-t >= 0
  d = s-t;
  mprintf("j=%10d k=%10d P=%s (%s)\n",j,k,string(d>=0),string(d));
end
mprintf("End\n");



