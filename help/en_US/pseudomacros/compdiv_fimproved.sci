// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_fimproved ( x, y )
    // Performs fast complex division with improved algorithm.
    //
    // Calling Sequence
    // r = compdiv_fimproved ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x/y.
    // 
    // Description
    // This implementation is based on the improved algorithm. 
    // This is a fast implementation, based on compiled source code.
    //
    // Compiling options are used to avoid the use of extended precision, and 
    // to strictly use SSE units, which results in predictable results.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_fimproved ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_fimproved ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    // Copyright (C) 2004 - Douglas M. Priest, Sun Microsystems
    //
    // Bibliography
    // "An improved complex division in Scilab", Michael Baudin, Robert L. Smith, 2011

endfunction

