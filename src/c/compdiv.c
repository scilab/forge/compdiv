// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include "compdiv.h"
#include <math.h>

#ifdef _MSC_VER
#include <float.h>
#include <stdlib.h>
#endif

// References
//
// "Efficient scaling for complex division"
// Douglas M. Priest
// Sun Microsystems
// ACM Transactions on Mathematical software, Vol. 30, No. 4, December 2004
//
// "An Improved Complex Division in Scilab"
// Michaël Baudin, Robert L. Smith, 2011

void compdiv_myminmax ( double a , double b , double * abmin, double * abmax );
void compdiv_mymax ( double a , double b, double * abmax );
void improved_internal(double a, double b, double c, double d, double *e, double *f);
void robust_internal(double a, double b, double c, double d, double *e, double *f);
void internal_compreal(double a, double b, double c, double d, double r, double t, double *e);
int compdiv_isfinite(double x);
double compdiv_scalbn(double x,long exp);
double compdiv_logb(double x);
double compdiv_fmax(double a, double b);
int compdiv_isnan(double x);
int compdiv_isinf(double x);
double compdiv_copysign(double x,double y);
void compdiv_switch(double *a, double *b);

// The positive infinity
#ifdef _MSC_VER
#define COMPDIV_INFINITY _FPCLASS_PINF
#else
#define COMPDIV_INFINITY INFINITY
#endif


/*
* Set z[0] + i z[1] := (x[0] + i x[1]) / (y[0] + i y[1]) assuming:
*
* 1. x[0], x[1], y[0], y[1] are all finite, and y[0], y[1] are not
* both zero
*
* 2. "int" refers to a 32-bit integer type, "long long int" refers
* to a 64-bit integer type, and "double" refers to an IEEE 754-
* compliant 64-bit floating point type
*/
void compdiv_priest(const double *x, const double *y, double *z)
{
    union {
        long long int i;
        double d;
    } aa, bb, cc, dd, ss;
    double a, b, c, d, t;
    int ha, hb, hc, hd, hz, hw, hs;
    /* name the components of x and y */
    a = x[0];
    b = x[1];
    c = y[0];
    d = y[1];
    /* extract high-order 32 bits to estimate |x| and |y| */
    aa.d = a;
    bb.d = b;
    ha = (aa.i >> 32) & 0x7fffffff;
    hb = (bb.i >> 32) & 0x7fffffff;
    hz = (ha > hb)? ha : hb;
    cc.d = c;
    dd.d = d;
    hc = (cc.i >> 32) & 0x7fffffff;
    hd = (dd.i >> 32) & 0x7fffffff;
    hw = (hc > hd)? hc : hd;
    /* compute the scale factor */
    if (hz < 0x07200000 && hw >= 0x32800000 && hw < 0x47100000) {
        /* |x| < 2^-909 and 2^-215 <= |y| < 2^114 */
        hs = (((0x47100000 - hw) >> 1) & 0xfff00000) + 0x3ff00000;
    } 
    else
    {
        hs = (((hw >> 2) - hw) + 0x6fd7ffff) & 0xfff00000;
    }
    ss.i = (long long int) hs << 32;
    /* scale c and d, and compute the quotient */
    c *= ss.d;
    d *= ss.d;
    t = 1.0 / (c * c + d * d);
    c *= ss.d;
    d *= ss.d;
    z[0] = (a * c + b * d) * t;
    z[1] = (b * c - a * d) * t;
    return;
}

//  ISO/IEC 9899:1999 (E) ©ISO/IEC

/*
* Set z[0] + i z[1] := (x[0] + i x[1]) / (y[0] + i y[1])
*/
void compdiv_ansiisoc(const double * x, const double * y, double * z )
{
	double a, b, c, d, logbw, denom, e, f;
	int ilogbw = 0;
	a = x[0]; 
	b = x[1];
	c = y[0]; 
	d = y[1];
	logbw = compdiv_logb(compdiv_fmax(fabs(c), fabs(d)));
	if (compdiv_isfinite(logbw)) 
	{
		ilogbw = (int)logbw;
		c = compdiv_scalbn(c, -ilogbw); 
		d = compdiv_scalbn(d, -ilogbw);
	}
	denom = c * c + d * d;
	e = compdiv_scalbn((a * c + b * d) / denom, -ilogbw);
	f = compdiv_scalbn((b * c - a * d) / denom, -ilogbw);
	/* Recover infinities and zeros that computed as NaN+iNaN; */
	/* the only cases are nonzero/zero, infinite/finite, and finite/infinite, ... */
	if (compdiv_isnan(e) && compdiv_isnan(f)) 
	{
		if ((denom == 0.0) &&
			(!compdiv_isnan(a) || !compdiv_isnan(b))) 
	        {
			e = compdiv_copysign(COMPDIV_INFINITY, c) * a;
			f = compdiv_copysign(COMPDIV_INFINITY, c) * b;
		}
		else if ((compdiv_isinf(a) || compdiv_isinf(b)) &&
			compdiv_isfinite(c) && compdiv_isfinite(d)) 
		{
			a = compdiv_copysign(compdiv_isinf(a) ? 1.0 : 0.0, a);
			b = compdiv_copysign(compdiv_isinf(b) ? 1.0 : 0.0, b);
			e = COMPDIV_INFINITY * ( a * c + b * d );
			f = COMPDIV_INFINITY * ( b * c - a * d );
		}
		else if (compdiv_isinf(logbw) &&
			compdiv_isfinite(a) && compdiv_isfinite(b)) 
		{
			c = compdiv_copysign(compdiv_isinf(c) ? 1.0 : 0.0, c);
			d = compdiv_copysign(compdiv_isinf(d) ? 1.0 : 0.0, d);
			e = 0.0 * ( a * c + b * d );
			f = 0.0 * ( b * c - a * d );
		}
	}
	z[0] = e;
	z[1] = f;
    return;
}

/*
* Set z[0] + i z[1] := (x[0] + i x[1]) / (y[0] + i y[1])
* Improved Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
*/
void compdiv_improved(const double *x, const double *y, double *z)
{
    double a, b, c, d;
    double r, t;
    double e, f;
    //
    a = x[0];
    b = x[1];
    c = y[0];
    d = y[1];
    if ( fabs(d) <= fabs(c) )
    {
        improved_internal( a, b, c, d, &e, &f);
    }
    else
    {
        improved_internal( b, a, d, c, &e, &f);
        f = -f;
    }
    z[0] = e;
    z[1] = f;
    return;
}
void improved_internal(double a, double b, double c, double d, double *e, double *f)
{
    double r, t;
    r = d/c;
    t = 1/(c + d * r);
    if (r != 0)
    {
        *e = (a + b * r) * t;
        *f = (b - a * r) * t;
    }
    else
    {
        *e = (a + d * (b/c)) * t;
        *f = (b - d * (a/c)) * t;
    }
}
    
/*
* Set z[0] + i z[1] := (x[0] + i x[1]) / (y[0] + i y[1])
* Robust Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
*/
void compdiv_robust(const double *x, const double *y, double *z)
{
	double a, b, c, d;
	double e, f;
	double maxAB;
	double maxCD;
	double B = 2;
	double S = 1;
	double OV = 1.79769313486231570e+308;
	double UN = 2.22507385850720138e-308;
	double eps = 2.220446049250313081e-16;
	double Be;
	double OV2 = 8.98846567431157854e+307; // OV/2;
	double UNBeps = 2.00416836000897277e-292; // UN*B/eps;
    a = x[0];
    b = x[1];
    c = y[0];
    d = y[1];
    //
    // Pre-scale the arguments.
    //
    // Compute min and max
    compdiv_mymax ( fabs(a) , fabs(b) , &maxAB );
    compdiv_mymax ( fabs(c) , fabs(d) , &maxCD );
    //    
    // Scaling
    if ( maxAB > OV2 )
    {
        // Scale down a, b
        a = a/2;
        b = b/2;
        S = S*2;
    }
    if ( maxCD > OV2 )
    {
        // Scale down c, d
        c = c/2;
        d = d/2;
        S = S/2;
    }
    if ( maxAB < UNBeps )
    {
        // Scale up a, b
		Be = B/(eps*eps);
        a = a*Be;
        b = b*Be;
        S = S/Be;
    }
    if ( maxCD < UNBeps )
    {
        // Scale up c, d
		Be = B/(eps*eps);
        c = c*Be;
        d = d*Be;
        S = S*Be;
    }
    //
    // Use internal algorithm
    if ( fabs(d) <= fabs(c) )
    {
        robust_internal(a, b, c, d, &e, &f);
    }
    else
    {
        robust_internal(b, a, d, c, &e, &f);
        f = -f;
    }
    // Scale back
    z[0] = e * S;
    z[1] = f * S;
    return;
}

void compdiv_myminmax ( double a , double b , double * minab, double * maxab )
{
	if ( a < b )
	{
		*minab = a;
		*maxab = b;
	}
	else
	{
		*minab = b;
		*maxab = a;
	}
	return;
}

void compdiv_mymax ( double a , double b , double * maxab )
{
	if ( a < b )
	{
		*maxab = b;
	}
	else
	{
		*maxab = a;
	}
	return;
}

/*
* Set e + i f := (a + i b) / (c + i d)
* when |d|<= |c|.
* Robust Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
* A robust complex division, taking into account 
* for overflows or underflows.
*/
void robust_internal(double a, double b, double c, double d, double *e, double *f)
{
	double r, t;
	double br, ar;
        r = d/c;
        t = 1/(c + d * r);
        internal_compreal(a, b, c, d, r, t, e);
        a = -a;
        internal_compreal(b, a, c, d, r, t, f);
}
/*
* Set e := Re ( (a + i b) / (c + i d) )
* when |d|<= |c|.
* Robust Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
* A robust complex division, taking into account 
* for overflows or underflows.
*/
void internal_compreal(double a, double b, double c, double d, double r, double t, double *e)
{
	double br;
        if (r != 0)
        {
            br = b*r;
            if ( br != 0 )
            {
                *e = (a + br ) * t;
            }
            else
            {
                *e = a * t + (b * t) * r;
            }
        }
        else
        {
            *e = (a + d * (b/c) ) * t;
        }
}
/*
* For MS Visual C Compatibility, I had to introduce some changes.
* See the "Floating point Support" of the Visual Studio C++ library.
* http://msdn.microsoft.com/en-us/library/y0ybw9fy(v=vs.71).aspx
*/

// Returns a nonzero value if its argument x is not infinite; 
// that is, if –INF < x < +INF. 
// Returns 0 if the argument is infinite or a NAN.
int compdiv_isfinite(double x) 
{
#ifdef _MSC_VER
	return _finite(x);
#else
	return isfinite(x);
#endif
}

// Returns the value of x * 2exp.
double compdiv_scalbn(double x,long exp) 
{
#ifdef _MSC_VER
	return _scalb(x,exp);
#else
	return scalbn(x,exp);
#endif
}

// Returns the unbiased exponential value of x.
double compdiv_logb(double x) 
{
#ifdef _MSC_VER
	return _logb(x);
#else
	return logb(x);
#endif
}

// Return larger of two values.
double compdiv_fmax(double a, double b) 
{
#ifdef _MSC_VER
	return __max(a,b);
#else
	return fmax(a,b);
#endif
}

// Returns a nonzero value if the argument x is a NAN; otherwise it returns 0.
int compdiv_isnan(double x)
{
#ifdef _MSC_VER
	return _isnan(x);
#else
	return isnan(x);
#endif
}

// Returns a non-zero value if x is an infinity (positive or negative).
int compdiv_isinf(double x)
{
#ifdef _MSC_VER
	if ( x==_FPCLASS_NINF || x==_FPCLASS_PINF)
	{
		return 1;
	}
	else
	{
		return 0;
	}
#else
	return isinf(x);
#endif
}

// Returns its double-precision, floating-point argument x with the same sign as 
// its double-precision, floating-point argument y.
double compdiv_copysign(double x,double y)
{
#ifdef _MSC_VER
	return _copysign(x,y);
#else
	return copysign(x,y);
#endif
}

/*
* Set z[0] + i z[1] := (x[0] + i x[1]) / (y[0] + i y[1])
*  "A Note on Complex Division", G. W. Stewart, University of Maryland, ACM Transactions on Mathematical Software, Vol. 11, No 3, September 1985, Pages 238-241
* Corrigendum: "A Note on Complex Division",G. W. Stewart, TOMS, Volume 12, Number 3, Pages = 285--285, September, 1986
*/
void compdiv_stewart(const double *x, const double *y, double *z)
{
	double a, b, c, d;
	double e, f;
	double s;
	double t;
	int flip;
    a = x[0];
    b = x[1];
    c = y[0];
    d = y[1];

    flip = 0;
    if ( fabs(d) >= fabs(c) ) 
    {
      compdiv_switch(&c,&d);
      compdiv_switch(&a,&b);
      flip=1;
    }
    s=1/c;
    t=1/(c+d*(d*s));
    if ( fabs(d) >= fabs(s) )
    {
      compdiv_switch(&s,&d);
    }
    if ( fabs(b) >= fabs(s) )
    {
      e=t*(a+s*(b*d));
    }
    else if ( fabs(b) >= fabs(d) )
    {
      e=t*(a+b*(s*d));
    }
    else
    {
      e=t*(a+d*(s*b));
    }
    if ( fabs(a) >= fabs(s) )
    {
      f=t*(b-s*(a*d));
    }
    else if ( fabs(a) >= fabs(d) )
    {
      f=t*(b-a*(s*d));
    }
    else
    {
      f=t*(b-d*(s*a));
    }
    if ( flip )
    {
      f=-f;
    }
    z[0] = e;
    z[1] = f;
    return;
}

void compdiv_switch(double *a, double *b)
{
  double t;
  t = *a;
  *a = *b;
  *b = t;
}


