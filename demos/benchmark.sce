// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// A performance test.
function n = scibench_searchforsize ( timemin , nfact , benchfun )
    // Returns the size of the problem for which the function is larger than timemin
    //
    // Example
    // stacksize("max");
    // function t=pascalup_col0rhs(n)
    // // Pascal up matrix.
    // // Column by column version
    // tic ()
    // c = eye(n,n)
    // c(1,:) = ones(1,n)
    // for i = 2:(n-1)
    // c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
    // end
    // t = toc()
    // endfunction
    // n = scibench_searchforsize ( 1 , 1.2 , pascalup_col0rhs )

    // Setup the callback
    funtyp = typeof(benchfun)
    if ( funtyp=="function" ) then
      __benchfun_f__ = benchfun
      __benchfun_args__ = list()
    elseif ( funtyp=="list" ) then
      __benchfun_f__ = benchfun(1)
      __benchfun_args__ = list(benchfun(2:$))
    end      

    //
    // Make a loop over n
    n = 1;
    k = 1;
    while ( %t )
        ierr = execstr("t = __benchfun_f__(n,__benchfun_args__(1:$))","errcatch");
        if ( ierr <> 0 ) then
            laerr = lasterror();
            msg = sprintf(gettext("%s: Error while running benchmark: %s."),..
              "matmul_searchforsize",laerr);
            warning(msg)
            break
        end
        if ( t > timemin ) then
            break
        end
        n = ceil(nfact * n);
    end
endfunction

function t=benchcompdivM ( N , divfun )
  a = grand(N,1,"def");
  b = grand(N,1,"def");
  c = grand(N,1,"def");
  d = grand(N,1,"def");
  x=complex(a,b);
  y=complex(c,d);
  tic ()
  z=divfun(x,y)
  t = toc()
  mcds=(N/1.e6)/t
  mprintf("N=%d, t=%f (s), MCDS=%.2f\n",N,t,mcds)
endfunction

//
// Use a callback to print the timings
function stop=myoutfun(k, mcds, N)
  stop = %f
  //mprintf("Run #%d, MCDS=%.2f\n",k,mcds)
endfunction
//
mprintf("Searching for a dataset size...\n");
stacksize("max");
timemin = 0.3;
nfact = 1.5;
//N = scibench_searchforsize ( timemin , nfact , list(benchcompdiv,compdiv_scilab) );
N = 1574802;
//
mprintf("N=%d\n",N);
mprintf("MCDS = Millions of Complex Divisions per Second\n");
mprintf("Benchmarking compdiv_scilab...\n");
[t,msg] = scibench_benchfun ( list(benchcompdivM,N,compdiv_scilab) , "compdiv_scilab", [] , [] , list(myoutfun,N) );
tm = mean(t);
MCDS= (N/1.e6)/tm;
mprintf("Min=%.4f, Max=%.4f, Mean=%.4f, MCDS=%.4f\n",min(t),max(t),tm,MCDS);
//
mprintf("Benchmarking compdiv_fimproved...\n");
[t,msg] = scibench_benchfun ( list(benchcompdivM,N,compdiv_fimproved) , "compdiv_fimproved", [] , [] , list(myoutfun,N) );
tm = mean(t);
MCDS= (N/1.e6)/tm;
mprintf("Min=%.4f, Max=%.4f, Mean=%.4f, MCDS=%.4f\n",min(t),max(t),tm,MCDS);
//
mprintf("Benchmarking compdiv_frobust...\n");
[t,msg] = scibench_benchfun ( list(benchcompdivM,N,compdiv_frobust) , "compdiv_frobust", [] , [] , list(myoutfun,N) );
tm = mean(t);
MCDS= (N/1.e6)/tm;
mprintf("Min=%.4f, Max=%.4f, Mean=%.4f, MCDS=%.4f\n",min(t),max(t),tm,MCDS);
//
mprintf("Benchmarking compdiv_priest...\n");
[t,msg] = scibench_benchfun ( list(benchcompdivM,N,compdiv_priest) , "compdiv_priest", [] , [] , list(myoutfun,N) );
tm = mean(t);
MCDS= (N/1.e6)/tm;
mprintf("Min=%.4f, Max=%.4f, Mean=%.4f, MCDS=%.4f\n",min(t),max(t),tm,MCDS);
//
mprintf("Benchmarking compdiv_fstewart...\n");
[t,msg] = scibench_benchfun ( list(benchcompdivM,N,compdiv_fstewart) , "compdiv_fstewart", [] , [] , list(myoutfun,N) );
tm = mean(t);
MCDS= (N/1.e6)/tm;
mprintf("Min=%.4f, Max=%.4f, Mean=%.4f, MCDS=%.4f\n",min(t),max(t),tm,MCDS);

// compdiv_scilab: 10 iterations, mean=0.319900 (s), min=0.306000 (s), max=0.427000 (s)
// compdiv_fimproved: 10 iterations, mean=0.291900 (s), min=0.288000 (s), max=0.305000 (s)
// compdiv_frobust: 10 iterations, mean=0.472600 (s), min=0.468000 (s), max=0.488000 (s)

