// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Search for the worst accuracy of the complex multiplication.
//

function [r,flag] = mulnaiveflag (x,y)

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    ac = a*c
    bd = b*d
    ad = a*d
    bc = b*c
    flag = (~isinf(ac) & ~isinf(bd) & ~isinf(ad) & ~isinf(bc) & ac<>0  & bd<>0  & ad<>0  & bc<>0 )
    r = complex(ac-bd,ad+bc)
endfunction

function s=myformatd(d)
    if ( isnan(d) | isinf(d) ) then
        s = msprintf("%10s",string(d))
    else
        s = msprintf("%+.3e",d)
    end
endfunction
function s=myformatcomp(d)
    s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
endfunction

function compdiv_verbosedefault ( k , x , y , q1 , q2 , digits )

    digits=assert_computedigits(q1,q2,2)
            na = int(log2(real(x)))
            nb = int(log2(imag(x)))
            nc = int(log2(real(y)))
            nd = int(log2(imag(y)))
    mprintf("#%d: (2^%5d+%%i*2^%5d) op (2^%5d+%%i*2^%5d), q1=%s, q2=%s, d=%.2f\n", ..
    k,na,nb,nc,nd,..
    myformatcomp(q1),myformatcomp(q2),digits);
endfunction


nmatrix = [-1074:1023;-1074:1023;-1074:1023;-1074:1023];
kmax = 10000;
m = size(nmatrix,"c")
if ( %f ) then
na = nmatrix(1,grand(1,kmax,"uin",1,m));
nb = nmatrix(2,grand(1,kmax,"uin",1,m));
nc = nmatrix(3,grand(1,kmax,"uin",1,m));
nd = nmatrix(4,grand(1,kmax,"uin",1,m));
a=2^na;
b=2^nb;
c=2^nc;
d=2^nd;
clear na
clear nb
clear nc
clear nd
else
a = rand(1,kmax,"def")*2;
b = rand(1,kmax,"def")*2;
c = rand(1,kmax,"def")*2;
d = rand(1,kmax,"def")*2;
end
//
// Compute random signs
sa=grand(1,kmax,"uin",0,1)*2-1;
sb=grand(1,kmax,"uin",0,1)*2-1;
sc=grand(1,kmax,"uin",0,1)*2-1;
sd=grand(1,kmax,"uin",0,1)*2-1;
a=a.*sa;
b=b.*sb;
c=c.*sc;
d=d.*sd;
//
// Create (x,y)
x=a+%i*b;
y=c+%i*d;
clear a
clear b
clear c
clear d
j=0
//
// Loop
for k = 1 : kmax
    if ( modulo(k,500)==0 ) then
    mprintf("k=%d\n",k);
    end
    [q1,flag] = mulnaiveflag (x(k),y(k));
    q2 = compdiv_mulscilab ( x(k) , y(k) );
    digits = compdiv_computedigits(q1,q2,2);
    if ( flag & digits < 53 ) then
        compdiv_verbosedefault ( k , x(k) , y(k) , q1 , q2 , digits );
    end
end

