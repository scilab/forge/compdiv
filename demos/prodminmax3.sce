// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [c,d]=switch(a,b)
  c=a
  d=b
endfunction
function p = prodminmax3 ( a, b, c )
    // Put the maximum absolute value into c
    // Put the minimum absolute value into a
    if ( abs(a) > abs(c) ) then
        [c,a]=switch(a,c)
    end
    if ( abs(b) > abs(c) ) then
        [c,b]=switch(b,c)
    end
    if ( abs(a) > abs(b) ) then
        [b,a]=switch(a,b)
    end
    p = (a*c)*b
endfunction

function p = compdiv_prodminmax ( x )
    // Computes the product of three variables, avoiding overflows or underflows.
    //
    // Calling Sequence
    // p = compdiv_prodminmax ( x )
    //
    // Parameters
    // x : a matrix of complex doubles
    // p : a matrix of complex doubles, the product x(1)*x(2)*...*x(n)
    //
    // Description
    // A stable floating point product of a vector.
    // Based on Stewart's theorem.
    //
    // Examples
    // p = compdiv_prodminmax ( [1.e200 1.e200 1.e-100] )
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    
    n = size(x,"*")
    if ( n<>3 ) then
        error("This function does work with three entries.")
    end
    p = prodminmax3 ( x(1), x(2), x(3) )
endfunction


x = [1.e308 1.e308 1.e-308];
p = compdiv_prodminmax ( x )

for k = 1:1000
  x = rand(3,1);
  p = compdiv_prodminmax ( x );
  e = prod(x);
  re = abs(p-e)/abs(e);
  mprintf("k=%d, re=%e\n",k,re)
  if ( re > 10*%eps) then
  error("Wrong result")
  end
end
  

