// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

/* -------------------------------------------------------------------------- */
/* This gateway uses the new scilab api
/* -------------------------------------------------------------------------- */

#include "machine.h"
#include "Scierror.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "localization.h"
#include "string.h"

#include "compdiv.h"
#include "gw_support.h"

// Consider a function Z=F(X,Y).
// where X and Y are both complex and n-ny-m matrices.
// Get the X and Y input arguments from the command line.
// Create the output arg Z as a complex n-by-m matrix.
int compdiv_getXYZ (char * fname, 
    int *rowsX, int *colsX, double** lrX, double** liX, 
    int *rowsY, int *colsY, double** lrY, double** liY, 
    double** lrZ, double** liZ) 
{

	SciErr sciErr;
	int *piAddr = NULL;
	int iType = 0;
	int iComplex = 0;
	int one=1;

	int minlhs=1, minrhs=2, maxlhs=1, maxrhs=2;

	CheckRhs(minrhs,maxrhs) ;
	if (! C2F(checkrhs)(fname, &minrhs, &maxrhs, (unsigned long)strlen(fname)) )
	{
		return 0;
	}
	if (! C2F(checklhs)(fname, &minlhs, &maxlhs, (unsigned long)strlen(fname)) )
	{
		return 0;
	}

	//
	// Read X
	//
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if ( iType != sci_matrix ) 
	{
		Scierror(999,_("%s: Wrong type for input argument #%d. Matrix expected.\n"),fname,1);
		return 0;
	}
	iComplex	= isVarComplex(pvApiCtx, piAddr);
	if ( iComplex == 0 ) 
	{
		Scierror(999,_("%s: Wrong content for input argument #%d. Complex matrix expected.\n"),fname,1);
		return 0;
	}
	sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, rowsX, colsX, lrX, liX);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//
	// Read Y
	//
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if ( iType != sci_matrix ) 
	{
		Scierror(999,_("%s: Wrong type for input argument #%d. Matrix expected.\n"),fname,1);
		return 0;
	}
	iComplex	= isVarComplex(pvApiCtx, piAddr);
	if ( iComplex == 0 ) 
	{
		Scierror(999,_("%s: Wrong content for input argument #%d. Complex matrix expected.\n"),fname,1);
		return 0;
	}
	sciErr = getComplexMatrixOfDouble(pvApiCtx, piAddr, rowsY, colsY, lrY, liY);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	//
	// Check that x and y have the same size
	//
	if ( *rowsX != *rowsY | *colsX != *colsY ) 
	{
		Scierror(999,_("%s: Arguments #%d and #%d must have the same size.\n"),fname,1,2);
		return 0;
	}
	//
	// Create Z
	//
	sciErr = allocComplexMatrixOfDouble(pvApiCtx,Rhs+1, *rowsX, *colsX, lrZ, liZ);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	return 1;
}

