// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

/* -------------------------------------------------------------------------- */
/* This gateway uses the new scilab api
/* -------------------------------------------------------------------------- */

#include "machine.h"
#include "Scierror.h"
#include "stack-c.h"
#include "api_scilab.h"
#include "localization.h"

#include "compdiv.h"

// z = compdiv_fstewart(x,y) returns the complex division x/y by 
// Robust algorithm.
int sci_compdiv_fstewart (char * fname) {

	int rowsX,colsX;
	double* lrX = NULL;
	double* liX = NULL;
	int rowsY,colsY;
	double* lrY = NULL;
	double* liY = NULL;
	double* lrZ = NULL;
	double* liZ = NULL;
	double x[2];
	double y[2];
	double z[2];
	int i, j;
	int ierr;

        ierr = compdiv_getXYZ (fname, 
        	&rowsX, &colsX, &lrX, &liX, 
		&rowsY, &colsY, &lrY, &liY, 
		&lrZ, &liZ);
	//
	// Perform the division
	//
	for(i = 0 ; i < rowsX ; i++)
	{
		for(j = 0 ; j < colsX ; j++)
		{
			x[0] = lrX[i + rowsX * j];
			x[1] = liX[i + rowsX * j];
			y[0] = lrY[i + rowsX * j];
			y[1] = liY[i + rowsX * j];
			compdiv_stewart(x, y, z);
			lrZ[i + rowsX * j] = z[0];
			liZ[i + rowsX * j] = z[1];
		}
	}

	LhsVar(1) = Rhs+1;

	return(0);

}

