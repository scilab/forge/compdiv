// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef __SCI_GW_SUPPORT_H__
#define __SCI_GW_SUPPORT_H__

int compdiv_getXYZ (char * fname, 
    int *rowsX, int *colsX, double** lrX, double** liX, 
    int *rowsY, int *colsY, double** lrY, double** liY, 
    double** lrZ, double** liZ);

#endif /* __SCI_GW_SUPPORT_H__ */
