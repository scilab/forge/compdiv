// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

expected = 11/25 + %i * 2/25;
z = compdiv_fstewart ( 1+2*%i , 3+4*%i );
assert_checkalmostequal(z,expected);
//
// Test on larger dataset.
ieee(2);
path = compdiv_getpath();
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
dg = compdiv_testdataset ( dataset , compdiv_fstewart );
assert_checktrue(mean(dg)>30);
//
// Test on difficult cases
ieee(2);
[x,y,z,q,digits] = compdiv_difficultcases(compdiv_fstewart);
expected=[53,53,53,0,53,53,0,0,0,5,0,53,0,53,0]';
assert_checktrue(floor(digits)==expected);
//
// Check that compdiv_stewart and compdiv_fstewart produce the same results.
// But fstewart is compiled, and faster.
//
ieee(2);
path = compdiv_getpath();
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
digits1 = compdiv_testdataset ( dataset , compdiv_stewart );
digits2 = compdiv_testdataset ( dataset , compdiv_fstewart );
assert_checkequal(digits1,digits2);

