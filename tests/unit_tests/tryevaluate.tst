// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

x = 1+2*%i;
y = 3+4*%i;
z = 11/25 + %i * 2/25;
for express = 1:8
    [g,m] = compdiv_tryevaluate ( x , y , express );
    if ( or(express==[1 3 5 7]) ) then
        assert_checkalmostequal(g,0.44,%eps);
    else
        assert_checkalmostequal(g,0.08,2*%eps);
    end
end

