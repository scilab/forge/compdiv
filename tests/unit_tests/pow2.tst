// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

F = [1/2 %pi/4 -3/4 1/2 1-%eps/2 1/2];
E = [1 2 2 -51 1024 -1021];
X = compdiv_pow2 ( F , E );
realmax = number_properties("huge");
realmin = number_properties("tiny");
expected = [1 %pi -3 %eps  realmax realmin];
assert_checkequal(X,expected);

