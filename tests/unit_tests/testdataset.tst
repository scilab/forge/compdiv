// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- NO CHECK REF -->
// <-- JVM NOT MANDATORY -->

ieee(2);
path = compdiv_getpath();
dataset = fullfile(path,"tests","unit_tests","cdiv.dataset.csv");
[digits,x,y,z,q] = compdiv_testdataset ( dataset , compdiv_smith );
ntests = size(digits,"*");
assert_checktrue(digits>=0);
assert_checktrue(digits<=53);
assert_checkequal(size(digits),[ntests,1]);
assert_checkequal(size(x),[ntests,1]);
assert_checkequal(size(y),[ntests,1]);
assert_checkequal(size(z),[ntests,1]);
assert_checkequal(size(q),[ntests,1]);
//
// Test only divisions by zero
[digits,x,y,z,q] = compdiv_testdataset ( dataset , compdiv_smith , 89:100);
assert_checktrue(digits>=0);
ntests = size(digits,"*");
assert_checkequal(size(digits),[ntests,1]);
assert_checkequal(size(x),[ntests,1]);
assert_checkequal(size(y),[ntests,1]);
assert_checkequal(size(z),[ntests,1]);
assert_checkequal(size(q),[ntests,1]);

//
// Enable verbose mode on the easy tests
[digits,x,y,z,q] = compdiv_testdataset ( dataset , compdiv_smith , 1:88,%t);
assert_checktrue(digits>=0);
ntests = size(digits,"*");
assert_checkequal(size(digits),[ntests,1]);
assert_checkequal(size(x),[ntests,1]);
assert_checkequal(size(y),[ntests,1]);
assert_checkequal(size(z),[ntests,1]);
assert_checkequal(size(q),[ntests,1]);

