// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

ieee(2);

stacksize("max");
defk = 1000;

// With default parameters
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab );
assert_checktrue(j>0.3*defk);
assert_checktrue(p>0.3);
assert_checkalmostequal(s,sqrt(0.49*(1-0.49)/defk),1.e-2);
assert_checkequal(k,defk);
//
// With verbose mode
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [],10,[],[],1);
assert_checkequal(k,10);
//
// With customized parameters
grand("setsd",0);
nmatrix = [
-1074:1023
-1074:1023
-1074:1023
-1074:1023
];
kmax = 100000;
rtol = 1.e-1;
atol = 0;
verbose = 0;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , kmax , rtol , atol , verbose );

// Display the worst cases
grand("setsd",0);
verbose = 2;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );

// Display all
grand("setsd",0);
verbose = 3;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );

// Try much harder cases.
grand("setsd",0);
nmatrix = [-1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023; -1074:-900 900:1023 ];
verbose = 2;
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );
grand("setsd",0);
compdiv_searchfordiff ( compdiv_smith , compdiv_scilab , nmatrix , 10 , rtol , atol , verbose );

// To explore the larger exponents:
nmatrix = [ 900:1023; 900:1023; 900:1023; 900:1023 ];
// To explore the smaller exponents:
nmatrix = [ -1074:-900; -1074:-900; -1074:-900; -1074:-900 ];

// Customize the verbose function
function tf=myverbose(i, k, x, y, q1, q2, p, s, state )
    tf = %f
    if (state=="verb") then
        if ( modulo(k,1000)==0 ) then
        pmin = p-1.96*s
        pmax = p+1.96*s
        mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
        end
    elseif ( state=="diff" ) then
        na = int(log2(real(x)))
        nb = int(log2(imag(x)))
        nc = int(log2(real(y)))
        nd = int(log2(imag(y)))
        mprintf("Difference identified\n");
        mprintf("(2^%d+%%i*2^%d)/(2^%d+%%i*2^%d)\n",na,nb,nc,nd);
        mprintf("  Div1=%s+%%i*%s\n",string(real(q1)),string(imag(q1)))
        mprintf("  Div2=%s+%%i*%s\n",string(real(q2)),string(imag(q2)))
    end
endfunction
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , myverbose );

// A verbose function with one line by message
function tf=myverbosebrief(i, k, x, y, q1, q2, p, s, state )
    function s=myformatd(d)
        if ( isnan(d) | isinf(d) ) then
            s = msprintf("%10s",string(d))
        else
            s = msprintf("%+.3e",d)
        end
    endfunction
    function s=myformatcomp(d)
        s = msprintf("(%12s,%12s)",myformatd(real(d)),myformatd(imag(d)))
    endfunction

    tf = %f
    if (state=="verb") then
        if ( modulo(k,1000)==0 ) then
        pmin = p-1.96*s
        pmax = p+1.96*s
        mprintf("k=%d, j=%d, p=%.3e, 95%%=[%.2e,%.2e]\n",k,j,p,pmin,pmax);
        end
    elseif ( state=="diff" ) then
        na = int(log2(real(x)))
        nb = int(log2(imag(x)))
        nc = int(log2(real(y)))
        nd = int(log2(imag(y)))
        mprintf("#%d (%d): (2^%5d+%%i*2^%5d)/(2^%5d+%%i*2^%5d), q1=%s, q2=%s\n", ..
        k,i,na,nb,nc,nd,..
        myformatcomp(q1),myformatcomp(q2));
    end
endfunction
grand("setsd",0);
compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , nmatrix , 10 , rtol , atol , myverbosebrief );


// By block
grand("setsd",0);
[j , p, s, k] = compdiv_searchfordiff ( compdiv_naive , compdiv_scilab , [] , [] , [] , [] , [] , %t );
assert_checktrue(j>0.3*defk);
assert_checktrue(p>0.3);
assert_checkalmostequal(s,sqrt(0.49*(1-0.49)/defk),1.e-2);
assert_checkequal(k,defk);

