// Copyright (C) 2010 - Robert L. Smith - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function g = compdiv_evaluate ( x , y , express , m )
    // Evaluate intermediate expressions different formulas.
    //
    // Calling Sequence
    // g = compdiv_evaluate ( x , y , express , m )
    //
    // Parameters
    // x : a 1-by-1 matrix of doubles, the numerator
    // y : a 1-by-1 matrix of doubles, the denominator
    // express : a 1-by-1 matrix of doubles, express=1,2,3,4,5,6,7,8 the expression.
    // m : a 1-by-1 matrix of doubles, m=1,2,3, 4, 5, 6, the method.
    // g : a 1-by-1 matrix of doubles, the evaluation of the expression.
    // 
    // Description
    // Provides many expressions for the division x/y.
    // In order to return the result, we evaluate the expressions 
    // r and t:
    // <itemizedlist>
    //     <listitem><para>
    //         If express=1, 2, 3, 4 evaluates r = d/c and t = 1/(c + d * r).
    //     </para></listitem>
    //     <listitem><para>
    //     If express=5, 6, 7, 8 evaluates r = c/d and t = 1/(c * r + d).
    //     </para></listitem>
    // </itemizedlist>
    // Then we return e or f, depending on the value of express.
    // <itemizedlist>
    //     <listitem><para>
    //         If express=1, returns e = (a + b * r) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=2, returns f = (b - a * r) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=3, returns e = (a + d * (b/c)) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=4, returns f = (b - d * (a/c)) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=5, returns e = (a * r + b) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=6, returns f = (b * r - a) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=7, returns e = (c * (a/d) + b) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If express=8, returns f = (c * (b/d) - a) * t.
    //     </para></listitem>
    // </itemizedlist>
    // Several methods are available.
    // <itemizedlist>
    //     <listitem><para>
    //         If m=1, uses the direct formula, e.g. e = (a + b * r) * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If m=2, uses the expanded formula, e.g. e = a*t + b * r * t.
    //     </para></listitem>
    //     <listitem><para>
    //         If m=3,4,5,6, uses the super-scaled formula, e.g. (1+(b/a)*r)/((c/a)+(d/a)*r).
    //         If m=3: scales by a, if m=4: scales by b, 
    //         if m=5: scales by c, if m=6: scales by d.
    //     </para></listitem>
    // </itemizedlist>
    //
    // Examples
    // x = 1+2*%i;
    // y = 3+4*%i;
    // z = 11/25 + %i * 2/25
    // // All formulas for e
    // for express = [1 3 5 7]
    //     for m = 1:6
    //         e = compdiv_evaluate ( x , y , express , m )
    //     end
    // end
    // // All formulas for f
    // for express = [2 4 6 8]
    //     for m = 1:6
    //         f = compdiv_evaluate ( x , y , express , m )
    //     end
    // end
    //
    // Authors
    // Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    select express
    case 1 then
        r = d/c
        t = 1/(c + d * r)
        select m
        case 1 then
            e = (a + b * r) * t
        case 2 then
            e = a*t + b * r * t
        case 3 then
            e = (1+(b/a)*r)/((c/a)+(d/a)*r) // Div by a
        case 4 then
            e = (a/b+r)/(c/b+(d/b)*r) // Div by b
        case 5 then
            e = ((a/c)+(b/c)*r)/(1+r*r) // Div by c
        case 6 then
            e = (a/d+(b/d)*r)/(1/r+r) // Div by d : Overflow ?
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = e
    case 2 then
        r = d/c
        t = 1/(c + d * r)
        select m
        case 1 then
            f = (b - a * r) * t
        case 2 then
            f = b*t - a * r * t
        case 3 then
            f = ((b/a)-r)/((c/a)+(d/a)*r) // Div by a
        case 4 then
            f = (1-(a/b)*r)/(c/b+(d/b)*r) // Div by b
        case 5 then
            f = ((b/c)-(a/c)*r)/(1+r*r) // Div by c
        case 6 then
            f = (b/d-(a/d)*r)/(1/r+r) // Div by d : overflow ?
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = f
    case 3 then
        r = d/c
        t = 1/(c + d * r)
        select m
        case 1 then
            e = (a + d * (b/c)) * t
        case 2 then
            e = a*t + d * (b/c) * t
        case 3 then
            e = (1+(d/a)*(b/c))/((c/a)+(d/a)*r) // Div by a
        case 4 then
            e = (a/b+r)/(c/b+(d/b)*r) // Div by b
        case 5 then
            e = ((a/c)+(d/c)*(b/c))/(1+r*r) // Div by c
        case 6 then
            e = (a/d+b/c)/(1/r+r) // Div by d : overflow ?
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = e
    case 4 then
        r = d/c
        t = 1/(c + d * r)
        select m
        case 1 then
            f = (b - d * (a/c)) * t
        case 2 then
            f = b*t - d * (a/c) * t
        case 3 then
            f = ((b/a)-(d/a)*(a/c))/((c/a)+(d/a)*r) // Div by a
        case 4 then
            f = (1-(d/b)*(a/c))/(c/b+(d/b)*r) // Div by b
        case 5 then
            f = ((b/c)-(d/c)*(a/c))/(1+r*r) // Div by c
        case 6
            f = (b/d-a/c)/(1/r+r) // Div by d : overflow ?
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = f
    case 5 then
        r = c/d
        t = 1/(c * r + d  )
        select m
        case 1 then
            e = (a * r + b) * t
        case 2 then
            e = a * r * t + b * t
        case 3 then
            e = (r+b/a)/((c/a)*r+d/a) // Div by a
        case 4 then
            e = ((a/b)*r+1)/((c/b)*r+d/b) // Div by b
        case 5 then
            e = ((a/c)*r+b/c)/(r+1/r)  // Div by c : overflow ?
        case 6 then
            e = ((a/d)*r+b/d)/(r*r+1) // Div by d
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = e
    case 6 then
        r = c/d
        t = 1/(c * r + d  )
        select m
        case 1 then
            f = (b * r - a) * t
        case 2 then
            f = b * r * t - a * t
        case 3 then
            f = ((b/a)*r-1)/((c/a)*r+d/a) // Div by a
        case 4 then
            f = (r-a/b)/((c/b)*r+d/b) // Div by b
        case 5 then
            f = ((b/c)*r-a/c)/(r+1/r) // Div by c : overflow ?
        case 6 then
            f = ((b/d)*r-a/d)/(r*r+1) // Div by d
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = f
    case 7 then
        r = c/d
        t = 1/(c * r + d  )
        select m
        case 1 then
            e = (c * (a/d) + b) * t
        case 2 then
            e = c * (a/d) * t + b * t
        case 3 then
            e = (r+b/a)/((c/a)*r+d/a) // Div by a 
        case 4 then
            e = ((c/b)*(a/d)+1)/((c/b)*r+d/b) // Div by b
        case 5 then
            e = (a/d+b/c)/(r+1/r) // Div by c : overflow ?
        case 6 then
            e = (r*(a/d)+b/d)/(r*r+1) // Div by d
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = e
    case 8 then
        r = c/d
        t = 1/(c * r + d  )
        select m
        case 1 then
            f = (c * (b/d) - a) * t
        case 2 then
            f = c * (b/d) * t - a * t
        case 3 then
            f = ((c/a)*(b/d)-1)/((c/a)*r+d/a) // Div by a
        case 4 then
            f = (r-a/b)/((c/b)*r+d/b) // Div by b
        case 5 then
            f = (b/d-a/c)/(r+1/r) // Div by c : overflow ?
        case 6 then
            f = ((c/d)*(b/d)-a/d)/(r*r+1) // Div by d
        else
            error (msprintf("Unknown method m=%d",m))
        end
        g = f
    else
        error (msprintf("Unknown case c=%d",c))
    end
endfunction

