// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_mulscilab ( x, y )
    // Performs the complex multiplication with Scilab star.
    //
    // Calling Sequence
    // r = compdiv_mulscilab ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x*y.
    // 
    // Description
    // This is a function which uses Scilab's * to perform the 
    // complex multiplication.
    // It allows straightforward comparisons between algorithms,
    // so that all the functions have the same header.
    //
    // Examples
    // expected = -5 + %i * 10
    // compdiv_mulscilab ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_mulscilab ( x , y )
    // abs(r - x.*y)./abs(x)
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin
    
  
  r = x.*y;
endfunction

