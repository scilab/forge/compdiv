// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function z = compdiv_smithR ( x,y )
    // Performs complex division with a Smith method, as implemented in R.
    //
    // Calling Sequence
    // z = compdiv_smithR (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    //
    // Description
    // This algorithms uses Smith's method and performs additionnal up and down scaling.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_smithR ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_smithR ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Bibliography
    //  svn.r-project.org/R/trunk/src/main/complex.c, Robert Gentleman, Ross Ihaka, The R Development Core Team, The R Foundation
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    for i = 1 : size(x,"r")
    for j = 1 : size(x,"c")
    z(i,j) = compdiv_smithRS ( x(i,j) , y(i,j) )
    end
    end
endfunction

function z = compdiv_smithRS ( x , y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        r = d/c
        den = c * (1 + r * r)
        e = (a + b * r) / den
        f = (b - a * r) / den
    else
        r = c/d
        den = d * (1 + r * r)
        e = (a * r + b) / den
        f = (b * r - a) / den
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction

