// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_mulansic (x,y)
    // Performs complex multiplication with a ANSI/ISO C99 algorithm.
    //
    // Calling Sequence
    // r = compdiv_mulansic (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex multiplication result r=x*y.
    //
    // Description
    // Performs complex multiplication with a ANSI/ISO C99 algorithm.
    //
    // Examples
    // expected = -5 + %i * 10
    // compdiv_mulansic ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_mulansic ( x , y )
    // abs(r - x.*y)./abs(x)
    //
    // // Failure
    // x = 2^  915+%i*2^  951
    // y = 2^  969+%i*2^  378
    // e = complex(%inf,%inf)
    // compdiv_mulansic(x,y)
    //
    // Bibliography
    //  "Scilab is not naive", Baudin, 2011
    //  ISO/IEC 9899 - Programming languages - C, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

n = size(x,"r")
m = size(x,"c")
for i = 1 : n
for j = 1 : m
r(i,j) = mulansic_internal(x(i,j),y(i,j))
end
end

endfunction

function r = mulansic_internal(x,y)
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    ac = a * c; 
    bd = b * d;
    ad = a * d; 
    bc = b * c;
    e = ac - bd; 
    f = ad + bc;

    if (isnan (e) & isnan (f)) then
        // Recover infinities that computed as NaN + iNaN.  */
        recalc = %f;
        if (isinf(a) | isinf(b)) then
            // z is infinite.  "Box" the infinity and change NaNs
            // in the other factor to 0.  */
            if ( isinf(a) ) then
                a = compdiv_copysign(1, a);
            else
                a = compdiv_copysign(0, a);
            end
            if ( isinf(b) ) then
                b = compdiv_copysign(1, b);
            else
                b = compdiv_copysign(0, b);
            end
            if (isnan(c)) then
                c = compdiv_copysign(0, c);
            end
            if (isnan(d)) then
                d = compdiv_copysign(0, d);
            end
            recalc = %t;
        end
        if (isinf (c) | isinf (d)) then
            // w is infinite.  "Box" the infinity and change NaNs
            // in the other factor to 0.  */
            if ( isinf(c) ) then
                c = compdiv_copysign(1, c);
            else
                c = compdiv_copysign(0, c);
            end
            if ( isinf(d) ) then
                d = compdiv_copysign(1, d);
            else
                d = compdiv_copysign(0, d);
            end
            if (isnan(a)) then 
                a = compdiv_copysign(0, a);
            end
            if (isnan(b)) then
                b = compdiv_copysign(0, b);
            end
            recalc = %t;
        end
        if (~recalc & (isinf(ac) | isinf(bd) | isinf(ad) | isinf(bc))) then
            // Recover infinities from overflow by changing NaNs to 0.  */
            if (isnan(a)) then
                a = copysign(0, a);
            end
            if (isnan(b)) then
                b = copysign(0, b);
            end
            if (isnan(c)) then
                c = copysign(0, c);
            end
            if (isnan(d)) then
                d = copysign(0, d);
            end
            recalc = %t;
        end
        if (recalc) then
            e = %inf * (a * c - b * d);
            f = %inf * (a * d + b * c);
        end
    end

    r = complex(e,f);
endfunction


