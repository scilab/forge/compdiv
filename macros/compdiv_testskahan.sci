// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,y,z,digits] = compdiv_testskahan ( div )
    // Test Kahan's complex divisions.
    //
    // Calling Sequence
    // [x,y,z,digits] = compdiv_testskahan ( div )
    //
    // Parameters
    // div : a function, the complex division under test. The function must have the header r=div(x,y).
    // x : a n-by-1 matrix of complex doubles, the numerator
    // y : a n-by-1 matrix of complex doubles, the denominator
    // z : a n-by-1 matrix of complex doubles, the exact complex division z=x./y
    // digits : a n-by-1 matrix of doubles, the number of binary digits for the tests.
    //
    // Description
    // This function tests the accuracy of the complex division function <literal>div</literal>
    // based on a special set of divisions.
    // These extreme divisions are inspired by Kahan in "Marketing versus Mathematics", 
    // in the Appendix:  Over/Underﬂow Undermines Complex Number Division in  Java.
    //
    // The text by Kahan is: 
    // "[Smith's formula] runs slower because of a third division; 
    // it still generates spurious over/underflows at extreme
    // arguments; and it spoils users’ test cases when z should ideally 
    // be real or pure imaginary ([note from me: this is case A]), or when
    // z, y and x = z·y are all small complex integers, but the computed z isn’t
    // ([note from me: this is case B])."
    //
    // In the current test set, there are n=5 tests.
    // The tests #1 and #2 are in case A and tests #3 to 5 are in case B.
    // 
    // Examples
    // format("e",25)
    // [x,y,z,digits] = compdiv_testskahan ( compdiv_naive )
    // compdiv_naive(x,y)
    //
    // // Test smith
    // [x,y,z,digits] = compdiv_testskahan ( compdiv_smith )
    // compdiv_smith(x,y)
    //
    // Bibliography
    //  "Marketing versus Mathematics, and other Ruminations on the Design of Floating-Point Arithmetic", W. Kahan, August 27, 2000"
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function digits = extreme_print(x,y,exact)
        c2 = div(x,y);
        digits = compdiv_computedigits(c2,exact,2)
        mprintf("x=(%.2e,%.2e) y=(%.2e,%.2e) E=(%+.3e,%+.3e) D=(%+.3e,%+.3e) bits=%.1f\n",..
        real(x),imag(x),..
        real(y),imag(y),..
        real(exact),imag(exact),..
        real(c2),imag(c2),..
        digits);
    endfunction
    //
    // Setup the tests
    //
    // Case A.
    //
    k = 1
    y(k) = 5. - 3.*%i;
    x(k) = 1.1*y(k);
    z(k) = 1.1
    //
    k = k+1
    y(k) = -5. - 3.*%i;
    x(k) = 1.1*y(k);
    z(k) = 1.1
    //
    // Case B
    //
    k = k+1
    x(k) = (27-21*%i);
    y(k) = (9-7*%i);
    z(k) = 3
    //
    k = k+1
    x(k) = (3-19*%i);
    y(k) = (1-3*%i);
    z(k) = 6-%i
    //
    k = k+1
    x(k) = (31-5*%i);
    y(k) = (3+5*%i);
    z(k) = 2-5*%i
    //
    // Do the tests
    //
    for k = 1 : 5
        digits(k) = extreme_print(x(k),y(k),z(k));
    end
endfunction

