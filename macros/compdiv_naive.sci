// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function r = compdiv_naive (x,y)
    // Performs complex division with a naive algorithm.
    //
    // Calling Sequence
    // r = compdiv_naive (x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the complex division result r=x/y.
    //
    // Description
    // The straightforward mathematical formula, which involves 
    // the denominator d = c^2 + d^2.
    // This is not an accurate implementation of the complex division.
    //
    // The cost is 2 floating point divisions, 6 floating point multiplications 
    // and 3 floating point additions.
    // The total cost is 11 floating point operations.
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_naive ( 1+2*%i , 3+4*%i )
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_naive ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    // Bibliography
    //  "Scilab is not naive", Baudin, 2010
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)

    den = c .* c + d .* d
    e = (a .* c + b .* d) ./ den
    f = (b .* c - a .* d) ./ den
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    r = complex(e,f)
endfunction

