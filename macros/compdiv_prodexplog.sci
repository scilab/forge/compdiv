// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = compdiv_prodexplog ( v )
    // Returns the product of the entries in x.
    //
    // Calling Sequence
    // p = compdiv_prodexplog ( v )
    //
    // Parameters
    // x : a matrix of doubles
    // p : a 1-by-1 matrix of doubles, the product
    //
    // Description
    // Combine exp and log to perform a robust product. 
    // This costs much more than frexp. 
    //
    // Examples
    // // Basic tests
    // x = [1.e308 1.e308 1.e-308 1.e-308]
    // expected = 1;
    // prod ( x )
    // compdiv_prodexplog ( x )
    //
    // p = compdiv_prodexplog ( [1 3 1 2] ) // 6
    // p = compdiv_prodexplog ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    // p = compdiv_prodexplog ( [-1 1 1] )   // -1
    // p = compdiv_prodexplog ( [1 -1 1] )   // -1
    // p = compdiv_prodexplog ( [1.e200 1.e200 1.e-100] ) //  1.+300
    //
    // // Accuracy test
    // x = [
    // -8207398118072301*2^-53
    //  8727187368991500*2^-54
    //  6237403295485065*2^-56
    // -6707097243752269*2^-55
    // -6829010895736168*2^-55
    //  5265283919956334*2^-53
    //  4521970942753754*2^-51
    // -6348554437180427*2^-53
    //  7523931345950583*2^-55
    // -5372571516972004*2^-52
    // ];
    // e = -0.277920000769307861379e-3;
    // r = (compdiv_prodexplog(x)-e)/e
    //
    // // Random test
    // x = rand(10,1,"normal");
    // r = abs(compdiv_prodexplog(x)-prod(x))/abs(prod(x))
    //
    // Authors
    // Copyright (C) 2010 - 2011 - Michael Baudin

    pe = exp(sum(log(v)))
    p = abs(pe) * sign(real(pe))
endfunction
