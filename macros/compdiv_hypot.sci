// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = compdiv_hypot ( a , b )
    //   Square root of sum of squares.
    //
    // Calling Sequence
    // c = compdiv_hypot ( a , b )
    //
    // Parameters
    // a : a matrix of complex doubles
    // b : a matrix of complex doubles
    // c : a matrix of complex doubles, the square root of the sum of squares.
    //
    // Description
    // Uses a floating point robust implementation of c = sqrt(abs(a).^2 + abs(b).^2).
    // The current implementation avoids unnecessary overflows.
    //
    // TODO : more extensive testing
    // TODO : check the shapes of a , b
    // TODO : expand a or b
    //
    // Examples
    // compdiv_hypot(1e153,1e153) // 1.41D+153
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    //
    // Bibliography
    // http://www.mathworks.com/help/techdoc/ref/hypot.html
    // http://en.wikipedia.org/wiki/Methods_of_computing_square_roots
    // http://mathworld.wolfram.com/SquareRootAlgorithms.html
    // "Replacing Square Roots by Pythagorean Sums", Cleve Moler and Donald Morrison, http://www.research.ibm.com/journal/rd/276/ibmrd2706P.pdf

    function c = compdiv_hypotS ( a , b )
        if (a==0) then
            c = abs(b)
        elseif (b==0) then
            c = abs(a)
        else
            if (abs(b)>abs(a)) then
                r = a/b
                t = abs(b)
            else
                r = b/a
                t = abs(a)
            end
            c = t * sqrt(1 + r^2)
        end
    endfunction

    for i = 1 : size(a,"r")
        for j = 1 : size(a,"c")
            c(i,j) = compdiv_hypotS ( a(i,j) , b(i,j) )
        end
    end
endfunction


