// Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = compdiv_robust ( x, y )
    // Improved Complex Division - Robert L. Smith - Michael Baudin - 2010-2011
    //
    // Calling Sequence
    // z = compdiv_robust ( x,y )
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // z : a matrix of complex doubles, the complex division result z=x/y.
    // 
    // Description
    // This implementation uses the improved complex division algorithm (as in compdiv_improved) and 
    // expanded expressions.
    // For example, we use the expression e = a * t + b * r * t instead 
    // of e = (a + b * r) * t.
    // This prevents overflows in sums. 
    // Moreover, we scale up c and d in the case where |t| is inf, in the same 
    // style as in Li et al, but better.
    // The best and the simplest so far. 
    //
    // Examples
    // expected = 11/25 + %i * 2/25
    // compdiv_robust ( 1+2*%i , 3+4*%i )
    //
    // // Exercize the robust division against difficult cases
    // compdiv_difficultcases ( compdiv_robust )
    //
    // // Exercize the robust division against Mc Laren cases
    // compdiv_maclaren ( compdiv_robust )
    //
    // // A failure case for the robust algorithm
    //  a=number_properties("huge")/64;
    // x=a+%i*a;
    // y=64*a+%i*a;
    // compdiv_scilab(x,y)
    // compdiv_robust(x,y)
    //
    // // Case 11 (another failure case)
    // x=2^-912+%i*2^-1029;
    // y=2^-122+%i*2^46;
    // z=2^-1074+%i*-2^-958;
    //
    // x = rand(3,5)+%i*rand(3,5)
    // y = rand(3,5)+%i*rand(3,5)
    // r = compdiv_robustS ( x , y )
    // abs(r.*y - x)./abs(x)
    //
    //
    // Authors
    // Copyright (C) 2010-2011 - Robert L. Smith - Michael Baudin

    for i = 1 : size(x,"r")
        for j = 1 : size(x,"c")
            z(i,j) = compdiv_robustS ( x(i,j) , y(i,j) )
        end
    end

endfunction
function z = compdiv_robustS ( x, y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    //
    // Pre-scale the arguments.
    AB = max(abs([a b]))
    CD = max(abs([c d]))
    // The choice of Li et al. for B:
    B = 2
    S = 1
    OV = number_properties("huge")
    UN = number_properties("tiny")
    Be = B/%eps^2
    // Scaling
    if ( AB >= OV/2 ) then
        // Scale down a, b
        x = x/2
        S = S*2
        //mprintf("Scale down a, b\n")
    end
    if ( CD >= OV/2 ) then
        // Scale down c, d
        y = y/2
        S = S/2
        //mprintf("Scale down c, d\n")
    end
    if ( AB < UN*B/%eps ) then
        // Scale up a, b
        x = x*Be
        S = S/Be
        //mprintf("Scale up a, b\n")
    end
    if ( CD < UN*B/%eps ) then
        // Scale up c, d
        y = y*Be
        S = S*Be
        //mprintf("Scale up c, d\n")
    end
    //
    // Use internal algorithm
    z = robust_internal(x,y)
    // Scale back
    z = z * S
endfunction
function z = robust_internal(x,y)
    // Set z := x/y
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        [e,f] = robust_subinternal(a,b,c,d)
    else
        [e,f] = robust_subinternal(b,a,d,c)
        f = -f
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    z = complex(e,f)
endfunction
function [e,f] = robust_subinternal(a,b,c,d)
    // Set e + i f := (a + i b) / (c + i d)
    // when |d|<= |c|.
    r = d/c
    t = 1/(c + d * r)
    e = internal_compreal(a,b,c,d,r,t)
    a = -a
    f = internal_compreal(b,a,c,d,r,t)
endfunction
function e = internal_compreal(a,b,c,d,r,t)
    // Set e := real ( (a + i b) / (c + i d) )
    // when |d|<= |c|.
    if (r <> 0) then
        br = b*r
        if ( br <> 0 ) then
            e = (a + br ) * t
        else
            e = a * t + (b * t) * r
        end
    else
        e = (a + d * (b/c) ) * t
    end
endfunction

