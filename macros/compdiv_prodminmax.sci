// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function p = compdiv_prodminmax ( x )
    // Computes the product, avoiding overflows or underflows.
    //
    // Calling Sequence
    // p = compdiv_prodminmax ( x )
    //
    // Parameters
    // x : a matrix of complex doubles
    // p : a matrix of complex doubles, the product x(1)*x(2)*...*x(n)
    //
    // Description
    // A stable floating point product of a vector.
    // Based on Stewart's theorem.
    //
    // Examples
    // p = compdiv_prodminmax ( [1.e200 1.e200 1.e-100] )
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin
    
    n = size(x,"*")
    x = matrix(x,n,1)
    for i = 1 : n - 1
        // Search the maximum absolute value
        [ma,kmax]=max(abs(x))
        // Search the minimum absolute value in other values
        o = [x(1:kmax-1);x(kmax+1:$)]
        [mi,komin]=min(abs(o))
        // komin is the index in o
        // Compute the index kmin in x.
        if ( komin<=kmax-1 ) then
            kmin = komin
        else
            kmin = komin+1
        end
        x(kmin) = x(kmax) * x(kmin)
        x(kmax) = []
    end
    p = x(1)
endfunction

// A stable floating point product of a vector.
// Based on Stewart's ideas
// Does not delete entries in the vector x.
// More clever, but longer: forces to generate the matrices (1:i).
function p = compdiv_prodminmax2 ( x )
    n = size(x,"*")
    x = matrix(x,n,1)
    for i = n-1 : -1 : 1
        // Search the maximum absolute value in x(1:i+1)
        [w,k]=max(abs(x(1:i+1)))
        // Switch the max and x(i+1)
        x([i+1,k]) = x([k,i+1])
        // Search the minimum absolute value in x(1:i)
        [w,k]=min(abs(x(1:i)))
        // Put the product into the min
        x(k) = x(k) * x(i+1)
    end
    p = x(1)
endfunction


